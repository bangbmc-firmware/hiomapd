#ifndef HIOMAPD_SOCKET_H
#define HIOMAPD_SOCKET_H

#include <stdint.h>
/* hiomapd unix socket cmd->husc
 * hiomapd unix socket transport->hust
 * some of these are redundant or won't ever
 * be implemented, but they are hear to mark was the only interface
 * provided*/
enum {
    husc_invalid = 0,
    /* This is an invalid message, it is only used on responses */
    husx_response = 1,
    /* control functions */
    husc_ping,
    husc_reset,
    husc_kill,
    husc_markflashmodified,
    husc_suspend,
    husc_resume,
    husc_setbackend,
    husc_daemonstate,
    husc_lpcstate,

    /* transport functions */
    hust_reset,
    hust_getinfo,
    hust_getflashinfo,
    hust_createreadwindow,
    hust_createwritewindow,
    hust_closewindow,
    hust_markdirty,
    hust_flush,
    hust_ack,
    hust_erase,
    /*v3 options? */
    hust_getflashname,
    hust_lock,
    /* Properties...
     * It may be simpler to just emita properties byte
     * over the socket interface
     */
    hust_flashcontrollost,
    hust_daemonready,
    hust_protocolreset,
    hust_windowreset,

    /* end of list marker */
    husx_api_limit
};

/*
 * Each request type will use a different number of args.
 * argument usage and placement is request defined.
 *
 */
struct unix_hiomapd_request {
    uint32_t request;
    uint32_t args[7];
};

/* request info blocks */
/* arguments who's type is smaller than uint32_t will be truncated down to size
 * so for example: a uin16_t in argument zero would only use the bottom 16 bit,
 * the next argument will be in arg1.
 * also note: these are all native endian encoded.
 *
 * responses: the first response argument is the status byte,
 * the second is the number of additional arguments (extranious?)
 */

/* hust_createreadwindow
 *
 * Takes two arguments:
 * arg[0]: window offset (uint16_t)
 * arg[1]: window size (uint16_t)
 *
 * TODO: how exactly does hiomapd use these values.
 * It looks like they are in units of 'page size'
 *
 */

/* hust_createwritewindow
 *
 * Takes two arguments:
 * arg[0]: window offset (uint16_t)
 * arg[1]: window size (uint16_t)
 *
 * TODO: how exactly does hiomapd use these values.
 * It looks like they are in units of 'page size'
 *
 */

/* hust_ack
 *
 * Take on argument:
 * arg[0]: flags
 *
 */
#endif
