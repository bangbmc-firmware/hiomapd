/* SPDX-License-Identifier: Apache-2.0 */
/* Copyright (C) 2018 IBM Corp. */

#ifndef TRANSPORT_H
#define TRANSPORT_H

struct mbox_context;
struct ops_container;
struct epoll_event;

struct transport_ops {
    int (*init)(struct mbox_context *, struct transport_ops *);
    int (*event)(struct ops_container *, struct epoll_event *);
    int (*fini)(struct ops_container *);

	int (*put_events)(struct ops_container *context, uint8_t mask);
	int (*set_events)(struct ops_container *context, uint8_t events,
			  uint8_t mask);
	int (*clear_events)(struct ops_container *context, uint8_t events,
			    uint8_t mask);
};

#define DECLARE_TRANSPORT_OPS(name) \
static struct transport_ops name##_transport_ops; \
static const  __attribute((section("ops_array"),used)) \
struct transport_ops *private_##name##_transport_ops_pointer = \
    & name##_transport_ops; \
static struct transport_ops name##_transport_ops

/* this is used to have a generic pointer, even
 * if it's embedded in a type other code may have no knowledge of.*/
struct ops_container {
    struct transport_ops *ops;
};

#define container_of(ptr, type, member) ({ \
    /* Store ptr in a var so it's not expanded multiple times, \
     * if it's an expression*/ \
    void *__l_ptr = (void*)(ptr); \
    ((type *)(__l_ptr - __builtin_offsetof(type, member))); })

/* We add EPOLLOUT so that the transport has a chance
 * to do some late init*/
#define register_event_fd(fd, ctx, payload) \
({ \
    struct epoll_event evt = { \
        .events = EPOLLIN | EPOLLOUT, \
        .data = { .ptr = payload, }, \
    }; \
    epoll_ctl(ctx->epollfd, EPOLL_CTL_ADD, fd, &evt); \
})
#endif /* TRANSPORT_H */
