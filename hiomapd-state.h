#ifndef HIOMAPD_STATE_H
#define HIOMAPD_STATE_H

/* Command Args */
/* Resume */
#define RESUME_NUM_ARGS		1
#define RESUME_NOT_MODIFIED	0x00
#define RESUME_FLASH_MODIFIED	0x01

/* Response Args */
/* Status */
#define DAEMON_STATE_NUM_ARGS	1
#define DAEMON_STATE_ACTIVE	0x00 /* Daemon Active */
#define DAEMON_STATE_SUSPENDED	0x01 /* Daemon Suspended */

/* LPC State */
#define LPC_STATE_NUM_ARGS	1
#define LPC_STATE_INVALID	0x00 /* Invalid State */
#define LPC_STATE_FLASH		0x01 /* LPC Maps Flash Directly */
#define LPC_STATE_MEM		0x02 /* LPC Maps Memory */


#endif
