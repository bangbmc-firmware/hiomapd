// SPDX-License-Identifier: Apache-2.0
// Copyright (C) 2018 IBM Corp.

#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <poll.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/signalfd.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/epoll.h>

#include "config.h"
#include "mboxd.h"
#include "common.h"
#include "backend.h"
#include "lpc.h"
#include "windows.h"
#include "vpnor/backend.h"

const char* USAGE =
	"\nUsage: %s [-V | --version] [-h | --help] [-v[v] | --verbose] [-s | --syslog]\n"
	"\t\t[-n | --window-num <num>]\n"
	"\t\t[-w | --window-size <size>M]\n"
	"\t\t-f | --flash <size>[K|M]\n"
#ifdef VIRTUAL_PNOR_ENABLED
	"\t\t-b | --backend <vpnor|mtd[:PATH]|file:PATH>\n"
#else
	"\t\t-b | --backend <mtd[:PATH]|file:PATH>\n"
#endif
	"\t-v | --verbose\t\tBe [more] verbose\n"
	"\t-s | --syslog\t\tLog output to syslog (pointless without -v)\n"
	"\t-n | --window-num\tThe number of windows\n"
	"\t\t\t\t(default: fill the reserved memory region)\n"
	"\t-w | --window-size\tThe window size (power of 2) in MB\n"
	"\t\t\t\t(default: 1MB)\n"
	"\t-f | --flash\t\tSize of flash in [K|M] bytes\n\n"
	"\t-t | --trace\t\tFile to write trace data to (in blktrace format)\n\n";


static int poll_loop(struct mbox_context *context)
{
	int rc = 0, i;
    int count;
    /* This is a low throughput api, 0x10 events is huge,
     * one would probably be sufficent.*/
    struct epoll_event evt[0x10] = { 0 };
    struct ops_container *w;

    while (true) {
        count = epoll_wait(context->epollfd, evt, 0x10, -1);
        if (count < 0)
        {
			MSG_ERR("Error from epoll_wait(): %s\n", strerror(errno));
            continue;
        }
        for (i = 0; i < count; i++)
        {
            w = evt[i].data.ptr;
            if (w[0].ops->event(w, evt + i) < 0)
			    MSG_ERR("Error in event dispatch\n");
        }
		if (context->terminate) {
			break; /* This should mean we clean up nicely */
		}
    }

	rc = protocol_reset(context);
	if (rc < 0) {
		MSG_ERR("Failed to reset during poll loop cleanup\n");
	}

	return rc;
}

static void usage(const char *name)
{
	printf(USAGE, name);
}

static bool parse_cmdline(int argc, char **argv,
			  struct mbox_context *context)
{
	char *endptr;
	int opt;

	static const struct option long_options[] = {
		{ "flash",		required_argument,	0, 'f' },
		{ "backend",		required_argument,	0, 'b' },
		{ "window-size",	optional_argument,	0, 'w' },
		{ "window-num",		optional_argument,	0, 'n' },
		{ "verbose",		no_argument,		0, 'v' },
		{ "syslog",		no_argument,		0, 's' },
		{ "trace",		optional_argument,	0, 't' },
		{ "version",		no_argument,		0, 'V' },
		{ "help",		no_argument,		0, 'h' },
		{ 0,			0,			0, 0   }
	};

	verbosity = MBOX_LOG_NONE;
	mbox_vlog = &mbox_log_console;

	context->current = NULL; /* No current window */

	while ((opt = getopt_long(argc, argv, "f:b:w::n::vst::Vh", long_options, NULL))
			!= -1) {
		switch (opt) {
		case 0:
			break;
		case 'f':
			context->backend.flash_size = strtol(optarg, &endptr, 10);
			if (optarg == endptr) {
				fprintf(stderr, "Unparseable flash size\n");
				return false;
			}
			switch (*endptr) {
			case '\0':
				break;
			case 'M':
				context->backend.flash_size <<= 10;
			case 'K':
				context->backend.flash_size <<= 10;
				break;
			default:
				fprintf(stderr, "Unknown units '%c'\n",
					*endptr);
				return false;
			}
			break;
		case 'b':
			context->source = optarg;
			break;
		case 'n':
			context->windows.num = strtol(argv[optind], &endptr,
						      10);
			if (optarg == endptr || *endptr != '\0') {
				fprintf(stderr, "Unparseable window num\n");
				return false;
			}
			break;
		case 'w':
			context->windows.default_size = strtol(argv[optind],
							       &endptr, 10);
			context->windows.default_size <<= 20; /* Given in MB */
			if (optarg == endptr || (*endptr != '\0' &&
						 *endptr != 'M')) {
				fprintf(stderr, "Unparseable window size\n");
				return false;
			}
			if (!is_power_of_2(context->windows.default_size)) {
				fprintf(stderr, "Window size not power of 2\n");
				return false;
			}
			break;
		case 'v':
			verbosity++;
			break;
		case 's':
			/* Avoid a double openlog() */
			if (mbox_vlog != &vsyslog) {
				openlog(PREFIX, LOG_ODELAY, LOG_DAEMON);
				mbox_vlog = &vsyslog;
			}
			break;
		case 'V':
			printf("%s V%s\n", THIS_NAME, PACKAGE_VERSION);
			exit(0);
		case 't':
			context->blktracefd = open(argv[optind],
						   O_CREAT|O_TRUNC|O_WRONLY,
						   0666);
			printf("Recording blktrace output to %s\n",
			       argv[optind]);
			if (context->blktracefd == -1) {
				perror("Couldn't open blktrace file for writing");
				exit(2);
			}
			break;
		case 'h':
			return false; /* This will print the usage message */
		default:
			return false;
		}
	}

	if (!context->backend.flash_size) {
		fprintf(stderr, "Must specify a non-zero flash size\n");
		return false;
	}

	MSG_INFO("Flash size: 0x%.8x\n", context->backend.flash_size);

	if (verbosity) {
		MSG_INFO("%s logging\n", verbosity == MBOX_LOG_DEBUG ? "Debug" :
					"Verbose");
	}

	return true;
}

static int mboxd_backend_init(struct mbox_context *context)
{
	const char *delim;
	const char *path;
	int rc;

	if (!context->source) {
		struct vpnor_partition_paths paths;
		vpnor_default_paths(&paths);

		rc = backend_probe_vpnor(&context->backend, &paths);
		if(rc < 0)
			rc = backend_probe_mtd(&context->backend, NULL);

		return rc;
	}

	delim = strchr(context->source, ':');
	path = delim ? delim + 1 : NULL;

	if (!strncmp(context->source, "vpnor", strlen("vpnor"))) {
		struct vpnor_partition_paths paths;

		if (path) {
			rc = -EINVAL;
		} else {
			vpnor_default_paths(&paths);
			rc = backend_probe_vpnor(&context->backend, &paths);
		}
	} else if (!strncmp(context->source, "mtd", strlen("mtd"))) {
		rc = backend_probe_mtd(&context->backend, path);
	} else if (!strncmp(context->source, "file", strlen("file"))) {
		rc = backend_probe_file(&context->backend, path);
	} else {
		rc = -EINVAL;
	}

	if (rc < 0)
		MSG_ERR("Invalid backend argument: %s\n", context->source);

	return rc;
}

extern void *__start_ops_array;
extern void *__stop_ops_array;
static struct transport_ops **ops_array =
    (struct transport_ops **)&__start_ops_array;
static size_t ops_array_size;

int main(int argc, char **argv)
{
	struct mbox_context *context;
	char *name = argv[0];
	int rc, i;
    ops_array_size =
        ((struct transport_ops **)&__stop_ops_array) -
        ((struct transport_ops **)&__start_ops_array);

	context = calloc(1, sizeof(*context));
	if (!context) {
		fprintf(stderr, "Memory allocation failed\n");
		exit(1);
	}

	if (!parse_cmdline(argc, argv, context)) {
		usage(name);
		free(context);
		exit(0);
	}

    context->epollfd = epoll_create1(EPOLL_CLOEXEC);

	MSG_INFO("Starting Daemon\n");

	rc = mboxd_backend_init(context);
	if (rc) {
		goto cleanup_context;
	}

	rc = protocol_init(context);
	if (rc) {
		goto cleanup_backend;
	}

	rc = lpc_dev_init(context);
	if (rc) {
		goto cleanup_mbox;
	}

	/* We've found the reserved memory region -> we can assign to windows */
	rc = windows_init(context);
	if (rc) {
		goto cleanup_lpc;
	}

    for (i = 0; i < ops_array_size; i++)
        ops_array[i]->init(context, ops_array[i]);

	/* Set the LPC bus mapping */
	__protocol_reset(context);

	/* We're ready to go, alert the host */
	context->bmc_events |= BMC_EVENT_DAEMON_READY;
	context->bmc_events |= BMC_EVENT_PROTOCOL_RESET;

	MSG_INFO("Entering Polling Loop\n");
	rc = poll_loop(context);

	MSG_INFO("Exiting Poll Loop: %d\n", rc);

	MSG_INFO("Daemon Exiting...\n");
	context->bmc_events &= ~BMC_EVENT_DAEMON_READY;
	context->bmc_events |= BMC_EVENT_PROTOCOL_RESET;

    /* I think we can skip all transport that are not the active
     * transport */

cleanup_windows:
	windows_free(context);
cleanup_lpc:
	lpc_dev_free(context);
cleanup_mbox:
	protocol_free(context);
cleanup_backend:
	backend_free(&context->backend);
cleanup_context:
	if (context->blktracefd)
		close(context->blktracefd);

	free(context);

	return rc;
}


/* Singal 'transport' */
struct signal_context {
    struct mbox_context *context;
    struct ops_container w;
    int signal_fd;
};

static inline struct signal_context* container_to_sctx(struct ops_container *container)
{
    return container_of(container, struct signal_context, w);
}

static int init_signals(struct mbox_context *context,
        struct transport_ops *ops)
{
	int rc;
	sigset_t set;

    struct signal_context *sctx = calloc(1, sizeof(*sctx));

    if (!sctx) {
		MSG_ERR("Failed to allocate signal context\n");
        return -ENOMEM;
    }
    sctx->context = context;
    sctx->w.ops = ops;

	/* Block SIGHUPs, SIGTERMs and SIGINTs */
	sigemptyset(&set);
	sigaddset(&set, SIGHUP);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGTERM);
	rc = sigprocmask(SIG_BLOCK, &set, NULL);
	if (rc < 0) {
		MSG_ERR("Failed to set SIG_BLOCK mask %s\n", strerror(errno));
        free(sctx);
		return rc;
	}

	/* Get Signal File Descriptor */
	rc = signalfd(-1, &set, SFD_NONBLOCK);
	if (rc < 0) {
		MSG_ERR("Failed to get signalfd %s\n", strerror(errno));
        free(sctx);
		return rc;
	}
    sctx->signal_fd = rc;

    register_event_fd(sctx->signal_fd, context, &sctx->w);
	return 0;
}

static int signal_event(struct ops_container *container,
        struct epoll_event *evt)
{
    struct signal_context *sctx = container_to_sctx(container);
    struct mbox_context *context = sctx->context;
    struct signalfd_siginfo info = { 0 };
    int rc;

    if (!(evt->events & EPOLLIN))
    {
		MSG_ERR("Signal event, but no event ready\n");
        return -EINVAL;
    }

    rc = read(sctx->signal_fd, (void *) &info,
          sizeof(info));

    if (rc != sizeof(info)) {
        MSG_ERR("Error reading signal event: %s\n",
            strerror(errno));
        return rc;
    }

    MSG_DBG("Received signal: %d\n", info.ssi_signo);
    switch (info.ssi_signo) {
        case SIGINT:
        case SIGTERM:
            MSG_INFO("Caught Signal - Exiting...\n");
            context->terminate = true;
            break;
        case SIGHUP:
            rc = protocol_reset(context);
            if (rc < 0) {
                MSG_ERR("Failed to reset on SIGHUP\n");
            }
            break;
        default:
            MSG_ERR("Unhandled Signal: %d\n",
                info.ssi_signo);
            break;
    }
    return 0;
}

DECLARE_TRANSPORT_OPS(signal) = {
    .init       = init_signals,
    .event      = signal_event,
    .fini       = 0,
	.put_events = 0,
	.set_events = 0,
	.clear_events = 0,
};
