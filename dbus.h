/* SPDX-License-Identifier: Apache-2.0 */
/* Copyright (C) 2018 IBM Corp. */

#ifndef DBUS_H
#define DBUS_H

#include <stdint.h>
#include <stddef.h>

/*
 * "mbox" will become an inappropriate name for the protocol/daemon, so claim a
 * different name on the public interface.
 *
 * "hiomapd" expands to "Host I/O Map Daemon"
 *
 * TODO: The Great Rename
 */
#define MBOX_DBUS_NAME			"xyz.openbmc_project.Hiomapd"
#define MBOX_DBUS_OBJECT		"/xyz/openbmc_project/Hiomapd"
#define MBOX_DBUS_CONTROL_IFACE		"xyz.openbmc_project.Hiomapd.Control"
#define MBOX_DBUS_PROTOCOL_IFACE	"xyz.openbmc_project.Hiomapd.Protocol"
#define MBOX_DBUS_PROTOCOL_IFACE_V2	MBOX_DBUS_PROTOCOL_IFACE ".V2"

/* Legacy interface */
#define MBOX_DBUS_LEGACY_NAME		"org.openbmc.mboxd"
#define MBOX_DBUS_LEGACY_OBJECT		"/org/openbmc/mboxd"

#include "hiomapd-state.h"

struct transport_dbus_context {
    struct ops_container w;
    struct mbox_context *context;
	sd_bus *bus;
    int dbus_fd;
};

static inline struct mbox_context *dctx_to_mctx(struct transport_dbus_context *dctx)
{
    return dctx->context;
}

static inline struct transport_dbus_context *
container_to_dctx(struct ops_container *container)
{
    return container_of(container, struct transport_dbus_context, w);
}

#endif /* MBOX_DBUS_H */
